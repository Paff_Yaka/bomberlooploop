using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Dialog : ScriptableObject
{
    public int dialogID;
    [System.Serializable]
    public class Message
    {
        [TextArea]
        public string text;
        public Sprite sprite;
        public bool trigger;
        public bool havestring;
        public bool reveal;
        public bool eva;
        public bool end;
        public string replacement;
    }

    public Message[] message;

    public int eventID;
    public int eventCase;
    [TextArea]
    public string Eventtext;
    public bool replace;
    public string truetext;
    public string falsetext;
    public string modifier;
    public int mamount;
    public Sprite Evensprite;
    public int addamount;
    [System.Serializable]
    public class keyword
    {
        public string[] EventKeyword;
        public bool neutral;
        public string addtype;
        public bool delay;
        public bool minus;
    }

    public keyword[] keywords;

    [TextArea]
    public string falseInput;

}
