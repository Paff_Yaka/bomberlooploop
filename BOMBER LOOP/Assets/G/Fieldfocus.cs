using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fieldfocus : MonoBehaviour
{
    public InputField Field;
    private bool wasFocused;

    [SerializeField] Dialogrunner dialogrunner;

    private void Update()
    {
        if (wasFocused && Input.GetKeyDown(KeyCode.Return))
        {
            Submit(Field.text);
        }

        wasFocused = Field.isFocused;
    }

    private void Submit(string text)
    {
        dialogrunner.txtinput(text);
    }
}
