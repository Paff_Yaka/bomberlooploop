using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    bool esc = false;
    [SerializeField] GameObject pause;
    [SerializeField] GameObject END;
    [SerializeField] GameObject main;
    [SerializeField] GameObject sprite;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!esc)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                main.SetActive(false);
                sprite.SetActive(false);
                pause.SetActive(true);
                esc = true;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                main.SetActive(true);
                pause.SetActive(false);
                sprite.SetActive(true);
                esc = false;
            }else if (Input.GetKeyDown(KeyCode.Y))
            {
                SceneManager.LoadScene(0);
            }
        }
    }

    public void ending()
    {
        main.SetActive(false);
        sprite.SetActive(false);
        pause.SetActive(false);
        END.SetActive(true);
        StartCoroutine(waitforscene());
    }

    IEnumerator waitforscene()
    {
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene(0);
    }

}
