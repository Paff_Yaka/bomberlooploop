using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataStore : MonoBehaviour
{
    public Dialog[] dialogs;

    public int Pint;
    public int Pstr;
    public int Pmor;
    public int Pmoney;

    public int Gpiss = 0;

    public string Name;
    public string year;
    public string motto;
    public string skills;

    public int num;

    [SerializeField] Text text;
    [SerializeField] Text nametext;
    [SerializeField] Text ragetext;

    private void Update()
    {
        nametext.text = "Name : " + Name;
        text.text = "Body : " + Pstr + "\n" + "Mind : " + Pint + "\n" + "Moral : " + Pmor + "\n" + "Wealth : " + Pmoney;
        ragetext.text = "GOD's Rage: " + Gpiss;
    }

    public void reveal()
    {
        Pint = Random.Range(1, 3);
        Pstr = Random.Range(1, 3);
        if(Pmor < 0)
        {
            int bp = Pmor;
            Pmor = Random.Range(1, 3);
            Pmor = Pmor + bp;
        }
        else
        {
            Pmor = Random.Range(1, 3);
        }

        text.gameObject.SetActive(true);
    }

    public void compare()
    {
        if(Pstr > Pint)
        {
            if(Pstr > Pmor)
            {
                num = 1;
            }
            else
            {
                num = 3;
            }
        }else if (Pint > Pmor)
        {
            num = 2;
        }
        else
        {
            num = 3;

        }
    }
    public void wealth()
    {
        if(Pmoney < 4)
        {
            num = 1;
        }else if (Pmoney > 7)
        {
            num = 3;
        }
        else
        {
            num = 2;
        }
    }
}
