using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;


public class Menumanager : MonoBehaviour
{
    [SerializeField] GameObject main;
    [SerializeField] GameObject how;
    [SerializeField] GameObject opt;
    [SerializeField] GameObject cred;

    [SerializeField] Text vol;
    [SerializeField] AudioMixer mix;
    [SerializeField] float volf = 1f;
    int casee = 0;
    int count = 10;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        switch (casee)
        {
            case 3:
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    main.SetActive(true);
                    cred.SetActive(false);
                    casee = 0;
                }
                break;
            case 2:

                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    volf -= 0.1f;
                    count -= 1;
                    if(volf < 0.1f)
                    {
                        count = 1;
                        volf = 0.1f; ;
                    }
                    mix.SetFloat("master", Mathf.Log10(volf) * 20);
                    vol.text = "MASTER VOLUME = " + count + "/10";
                }
                else if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    volf += 0.1f;
                    count += 1;
                    if (volf >= 1)
                    {
                        count = 10;
                        volf = 1;
                    }
                    mix.SetFloat("master", Mathf.Log10(volf) * 20);
                    vol.text = "MASTER VOLUME = " + count + "/10";
                }

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    main.SetActive(true);
                    opt.SetActive(false);
                    casee = 0;
                }
                break;
            case 1:
                if (Input.GetKeyDown(KeyCode.P))
                {
                    loadmain();
                }
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    main.SetActive(true);
                    how.SetActive(false);
                    casee = 0;
                }
                break;
            case 0:
                if (Input.GetKeyDown(KeyCode.P))
                {
                    main.SetActive(false);
                    how.SetActive(true);
                    casee = 1;
                }
                if (Input.GetKeyDown(KeyCode.O))
                {
                    main.SetActive(false);
                    opt.SetActive(true);
                    casee = 2;
                }
                if (Input.GetKeyDown(KeyCode.C))
                {
                    main.SetActive(false);
                    cred.SetActive(true);
                    casee = 3;
                }
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    Application.Quit();
                }
                break;
        }
        
        
    }

    public void loadmain()
    {
        SceneManager.LoadScene(1);
    }
}
