using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour {

	public float countdown = 2f;

	[SerializeField] Collider2D thiscol;

	[SerializeField] string bombtype;
	

    private void Start()
    {
		thiscol = GetComponent<Collider2D>();
		thiscol.enabled = !thiscol.enabled;

		
    }

    // Update is called once per frame
    void Update () {
		countdown -= Time.deltaTime;

		if (countdown <= 0f)
		{
			FindObjectOfType<MapDestroyer>().Explode(transform.position, bombtype);
			Destroy(gameObject);
		}else if (countdown <= 1.5f)
        {
			thiscol.enabled = !thiscol.enabled;
		}
	}
}
