using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIcon : MonoBehaviour
{
    [SerializeField] Text HPtext;
    [SerializeField] Text bombtext;

    [SerializeField] Text countt;

    [SerializeField] PlayerController PC;
    [SerializeField] BombSpawner BS;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        HPtext.text = "HP: " + PC._HP;
        bombtext.text = "Bomb: " + BS._bomblimit;
        countt.text = "Count: " + BS.count;
    }

}
