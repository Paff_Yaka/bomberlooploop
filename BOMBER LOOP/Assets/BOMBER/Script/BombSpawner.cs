using UnityEngine;
using UnityEngine.Tilemaps;

public class BombSpawner : MonoBehaviour {

	public Tilemap tilemap;

	public GameObject bombPrefab;

	[SerializeField] float Basedelay = 1f;
	[SerializeField] float delay;

	[SerializeField] float Basebombrecharge;
	[SerializeField] float bombrecharge;
	[SerializeField] int bomblimit;
	public int _bomblimit => bomblimit;
	public int count = 0;

	[SerializeField] int maxbomb;

	[SerializeField] GameObject player;
	
	// Update is called once per frame
	void Update () {
		
		if (delay <= 0)
        {
			if (bomblimit != 0) 
			{
				if (Input.GetKey(KeyCode.Space))
				{
					bomblimit -= 1;
					Vector3 worldPos = player.transform.position;
					Vector3Int cell = tilemap.WorldToCell(worldPos);
					Vector3 cellCenterPos = tilemap.GetCellCenterWorld(cell);

					Instantiate(bombPrefab, cellCenterPos, Quaternion.identity);
					delay = Basedelay;
					count += 1;
				}
			}
		}
        else
        {
			delay -= Time.deltaTime;
        }

		if(bomblimit != maxbomb)
        {
			
			bombrecharge -= Time.deltaTime;
        }
        else
        {
			bombrecharge = Basebombrecharge;
		}
		

		if (bombrecharge <= 0)
        {
			bomblimit += 1;
			if(bomblimit > maxbomb)
            {
				bomblimit = maxbomb;
            }

			bombrecharge = Basebombrecharge;
        }
	}
}
