using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] int MHP;
    public int _MHP => MHP;
    [SerializeField] int HP;
    public int _HP => HP;
    [SerializeField] int str;
    [SerializeField] int _damage;
    public int Bombdamage => _damage;
    [SerializeField] int AnimCode;

    [SerializeField] float dmgdelay;
    [SerializeField] float basedmgdelay;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        dmgdelay -= Time.deltaTime;
        if (Input.GetKey(KeyCode.W))
        {
            this.transform.rotation = Quaternion.Euler(0, 0, 0);
            this.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x, this.gameObject.transform.position.y + speed * Time.deltaTime);
        } 
        else if (Input.GetKey(KeyCode.S))
        {
            this.transform.rotation = Quaternion.Euler(0, 0, 180);
            this.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x, this.gameObject.transform.position.y - speed * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            this.transform.rotation = Quaternion.Euler(0, 0, 90);
            this.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x - speed * Time.deltaTime, this.gameObject.transform.position.y);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            this.transform.rotation = Quaternion.Euler(0, 0, 270);
            this.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x + speed * Time.deltaTime, this.gameObject.transform.position.y);
        }
    }

    public void inflictdamage(int damage)
    {
        if(dmgdelay <= 0f)
        {
            HP -= damage;
            dmgdelay = basedmgdelay;
        }
        
    }
}
