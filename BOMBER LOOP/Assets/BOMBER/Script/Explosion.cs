using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    [SerializeField] int dmg;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Selfdestroy());

        dmg = FindObjectOfType<PlayerController>().Bombdamage;
    }

    private IEnumerator Selfdestroy()
    {
        yield return new WaitForSeconds(0.3f);
        DestroyImmediate(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            FindObjectOfType<PlayerController>().inflictdamage(dmg);
        }
    }
}
